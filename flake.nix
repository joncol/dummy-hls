{
  description = "Flake utils example";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    hls = {
      inputs.flake-utils.follows = "flake-utils";
      url =
        "github:haskell/haskell-language-server?rev=8f1a59cf5b691e6a8a4b00aee9c8b8c2d619b84e";
    };
  };

  outputs = { self, nixpkgs, flake-utils, hls }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; };
      in rec {
        packages = flake-utils.lib.flattenTree {
          hello = pkgs.hello;
          gitAndTools = pkgs.gitAndTools;
        };
        defaultPackage = packages.hello;
        apps.hello = flake-utils.lib.mkApp { drv = packages.hello; };
        defaultApp = apps.hello;
        devShell = pkgs.mkShell rec {
          packages = with pkgs; [
            (hls.packages.${system}."haskell-language-server-8107")
            # (hls.packages.${system}."haskell-language-server-922")
            nyancat
          ];
        };
      });
}
